# 概要

SassとPugとES6をgulpで自動ビルド、画像の圧縮などのWebサイト制作に役立つ機能があります。

CSS構成は[FLOCSS](https://github.com/hiloki/flocss)を想定

node.jsを使用する

## Setup

コマンドラインから下記を入力

```
$ yarn install
```

※「$」はコマンドラインを表しているだけですので、入力不要です。



## 機能


### 自動ビルド

```
$ yarn -s run gulp
```

1. src配下にあるファイルを監視して更新を検知すると、dist配下にビルドします

2. ブラウザが立ち上がり、ビルドされたファイルを確認できます。



### デプロイ

1. sftp-config.jsonにサーバー情報を入力する

```json
sftp-config.json
{
  "host": "hogehoge.com",
  "user": "hoge",
  "pass": "hoge",
  "remotePath": "/htdocs/"
}
```

2. dist配下のファイルを任意のサーバへアップロードします。

```
$ yarn -s run gulp upload
```

