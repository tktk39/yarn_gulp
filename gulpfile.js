const gulp = require('gulp'),
  notifier = require('node-notifier'),
  browserSync = require('browser-sync').create(),
  mqpacker = require('css-mqpacker'),
  autoprefixer = require('autoprefixer'),
  cleanCSS = require('gulp-clean-css'),
  $ = require("gulp-load-plugins")();

// ディレクトリ
var dir = {
  src: 'src/',
  dist: 'dist/'
}

// errorを拾ったとき用のメッセージ
const errorHandler = (error) => {
  notifier.notify({
    message: 'error',
    title: error.message,
  }, () => {
    console.log(error.message);
  });
};

// gulp-sftp
config = require('./sftp-config.json');
gulp.task('upload', () => {
  return gulp.src(dir.dist + '*')
    .pipe($.sftp({
      host: config['host'],
      user: config['user'],
      pass: config['pass'],
      remotePath: config['remotePath']
    }));
});

// browser-sync
gulp.task('server', () => {
  return browserSync.init({
    server: {
      baseDir: './dist'
    }
  });
});

// babel
gulp.task('babel', () => { 
  return gulp.src(dir.src + 'js/*.js') 
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.sourcemaps.init()) 
    .pipe($.babel({
      presets: ['@babel/preset-env']
    }))
    .pipe($.uglify())
    .pipe($.sourcemaps.write('../maps/'))
    .pipe(gulp.dest(dir.dist+'assets/js/'))
    .pipe(browserSync.stream());
});

// pug
gulp.task('pug', () => {
  return gulp.src(['src/pug/**/*.pug', '!src/pug/**/_*.pug'])
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.pug({
    pretty: true
  }))
    .pipe(gulp.dest(dir.dist))
  .pipe(browserSync.stream());
});

// sass
gulp.task('sass',() => {
  var plugins = [
    // ベンダープレフィックス付与 対象： https://browserl.ist/?q=last+2+versions%2C+ie+%3E%3D+11%2C+iOS+%3E%3D+10
    autoprefixer({ browsers: 'last 2 versions, ie >= 11, iOS >= 10' }),
    // メディアクエリの整理
    mqpacker({ sort: true })
  ];
  return gulp.src(dir.src + 'scss/**/*.scss')
    .pipe($.plumber({ errorHandler: errorHandler }))
    .pipe($.sourcemaps.init())
    .pipe($.sass({ errLogToConsole: true }))
    .on('error', catchErr)
    .pipe($.sass())
    .pipe($.postcss(plugins))
    .pipe(cleanCSS())
    .pipe($.sourcemaps.write("../maps/"))
    .pipe(gulp.dest(dir.dist + 'assets/css/'))
    .pipe(browserSync.stream());
});

function catchErr(e) {
  console.log(e.formatted);
  this.emit('end');
}


// 画像圧縮
const mozjpeg = require('imagemin-mozjpeg'),
  pngquant = require('imagemin-pngquant');
gulp.task('img', () => {
  return gulp.src([
    dir.src + 'img/**/*'
  ])
  .pipe($.changed(dir.dist + 'img/'))
  // 画像圧縮処理
  .pipe($.imagemin([
    $.imagemin.gifsicle(),
    mozjpeg({ quality: 80 }),
    pngquant(),
    $.imagemin.svgo()],
    {
      verbose: true
    }
  ))
  // 出力先ディレクトリ
  .pipe(gulp.dest(dir.dist + 'assets/img/'))
  .pipe(browserSync.stream());
});

// ファイル変更監視
gulp.task('watch',() => {
  gulp.watch(dir.src + 'scss/**/*.scss', ['sass']);
  gulp.watch(dir.src + 'pug/**/*.pug', ['pug']);
  gulp.watch(dir.src + 'js/*.js', ['babel']);
  gulp.watch(dir.src + 'img/**/*', ['img']);
});

// 標準タスク
gulp.task('default', ['server', 'watch']);